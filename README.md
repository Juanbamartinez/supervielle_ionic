Install ionic
- npm install -g cordova ionic

Info and docs
- https://ionicframework.com/getting-started/

1. npm install
2. for run on browser and deploy: ionic serve
3. build project: ionic cordova build [platform]

- Add platform: cordova platform add xxx ó ionic cordova platform add xxx
- Add plugin: cordova plugin add xxx ó ionic cordova plugin add xxx
