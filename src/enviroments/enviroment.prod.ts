export const environment = {
  production: true,
  SERVICES_PATH: 'https://mobile.supervielle.com.ar/backendsvcs-dmz/svc/',
  PREFIX: 'PROD'
};
