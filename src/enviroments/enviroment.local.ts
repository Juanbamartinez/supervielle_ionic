export const environment = {
  production: false,
  SERVICES_PATH: 'https://mobiledesa.supervielle.com.ar/backendsvcs-dmz/svc/',
  PREFIX: 'DESA'
};
