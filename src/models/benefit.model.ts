export class Benefit {
  rubro: string;
  marca: string;
  logo: string;
  descuentoCuotas: string;
  tarjeta: string;
  direccion: string;
  lat: number;
  lng: number;
  distance: number;
  identite: string;
  raw_item: { any };
  nombre: string;
  vigencia: {
    desde: string;
    hasta: string
  };
  idBeneficio: string;
  legales: string;
  paginaWeb: string;
}
