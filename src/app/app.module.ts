import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { CommonModule } from "@angular/common";
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation  } from '@ionic-native/geolocation';
import { Device  } from '@ionic-native/device';
import { AgmCoreModule } from '@agm/core';

import { MyApp } from './app.component';
import { ComponentsModule } from './../components/components.module';
import { PagesModule } from './../pages/pages.module';
import { ServicesModule } from './../services/services.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { MenuComponent } from './../components/menu/menu';
import { TokenInterceptor } from './../services/token.interceptor';

@NgModule({
  declarations: [
    MyApp,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBiY6pfQ88ElJyy4k9Jx4Ej7NHOnztbWOU'
    }),
    ComponentsModule,
    PagesModule,
    HttpClientModule,
    ServicesModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MenuComponent
  ],
  providers: [
    Geolocation,
    Device,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    // TokenInterceptor se convierte en provider para que Angular pueda utilizarlo
    // HTTP_INTERCEPTORS --> se añade el intercepto al token de angular
    // useClass --> el HttpInterceptor
    // multi --> en true para aceptar más de un elemento y formar una cadena de interceptores
    // El orden en la declaración de providers va a determinar el orden de la cadena de interceptores
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
