import { Injectable } from '@angular/core';
import { SUPERVIELLE_ENDPOINTS } from './../constants/endpoint.constant';
import { HttpService } from './http.service';
import { Observable } from "rxjs";

@Injectable()
export class BenefitService {
  constructor(private httpService: HttpService) {
  }

  public getBenefitList(_zona: any): Observable<any> {
    // Observable.fromPromise --> convertir una promesa a un objeto Observable. Para trabajar sólo con elementos "Observable"
    // flatMap --> concatenar Observable ("cola" de Observables con su respectiva resolución). No es asíncrono sino que el último Observable de la cola se ejecuta sólo si el anterior fue resuelto
    return this.httpService.request(SUPERVIELLE_ENDPOINTS.CODE_BENEFIT_LIST, { zona: _zona })
  }

  public getBenefitDetail(_idBeneficio: string): Observable<any> {
    return this.httpService.request(SUPERVIELLE_ENDPOINTS.CODE_BENEFIT_DETAIL, { idBeneficio: _idBeneficio })
  }
}
