import { Injectable } from '@angular/core';
import { SUPERVIELLE_ENDPOINTS } from './../constants/endpoint.constant';
import { HttpService } from './http.service';
import { Observable } from "rxjs";

@Injectable()
export class NewsService {
  constructor(private httpService: HttpService) {
  }

  public getNewsList(_zona: any): Observable<any> {
    return this.httpService.request(SUPERVIELLE_ENDPOINTS.CODE_NEWS_LIST, { zona: _zona })
  }

  public getNewsDetail(_hashNovedad: string): Observable<any> {
    return this.httpService.request(SUPERVIELLE_ENDPOINTS.CODE_NEWS_DETAIL, { hashNovedad: _hashNovedad })
  }
}
