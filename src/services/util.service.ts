import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { Device } from '@ionic-native/device';
import { Storage } from '@ionic/storage';
import { LoadingController, Loading } from 'ionic-angular';
import { AlertService } from './alert.service';

@Injectable()
export class UtilService {
  private loading: any;

  constructor(
    private alertCtrl: AlertService, private loadCtrl: LoadingController, private geolocation: Geolocation,
    private storage: Storage, private device: Device
  ) {
  }

  public getCurrentPosition(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.geolocation.getCurrentPosition()
        .then((position) => {
          resolve(position);
        })
        .catch((error) => {
          let result = {
            codigo: "ERR",
            descripcion: "No fue posible obtener su ubicación. Verifique que tenga activo el servicio de Localización para esta aplicación.",
            tipoRespuesta: "ERROR"
          };
          reject(result);
        });
    });
  }

  public getDataModuleList(module?: string, func?: string, param?: string): any {
    let data: any;
    return new Promise((resolve, reject) => {
      this.storage.get('moduleList')
        .then((modules) => {
          for (let m of modules) {
            if (m.id === module) {
              for (let f of m.listaFuncionalidades) {
                if (f.nombre.toLowerCase() === func.toLowerCase()) {
                  data = f;
                  if (param) {
                    for (let p of f.listaParametro) {
                      if (p.nombre.toLowerCase() === param) {
                        data = p;
                      }
                    }
                  }
                }
              }
            }
          }
          resolve(data);
        })
        .catch(() => {
          let result = {
            codigo: "ERR",
            descripcion: "No fue posible obtener los datos solicitados.",
            tipoRespuesta: "ERROR"
          };
          reject(result);
        });
    });
  }

  public getDistanceFromLatLngInKm(lat, lng, lat2, lng2): number {
    function deg2rad(deg) {
      return deg * (Math.PI / 180);
    }
    let R = 6371; // Radio de la Tierra en KM
    let dLat = deg2rad(lat2 - lat);
    let dLng = deg2rad(lng2 - lng);
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat)) * Math.cos(deg2rad(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c; // Distancia km
    return d;
  }

  public getDeviceInfo(): any {
    return {
      uuid: this.getUUID(),
      platform: this.getPlatform(),
      model: this.getModel(),
      version: this.getVersion()
    }
  }

  public getUUID(): string {
    if (navigator.userAgent.indexOf("Firefox") === -1 || navigator.userAgent.indexOf("obile") != -1) {
      return "JBDEVICE00001";
    } else {
      return this.device.uuid;
    }
  }

  public getPlatform(): string {
    return this.device.platform;
  }

  public getModel(): string {
    return this.device.model;
  }

  public getVersion(): string {
    return this.device.model;
  }

  public getLoading(): Loading {
    return this.loadCtrl.create({
      content: "Loading"
    });
  }
}
