import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { HttpService } from './http.service';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private httpService: HttpService, private storage: Storage) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let WS = request.url.split('svc/')[1];
    if (WS === '1') {
      request = request.clone({
        setHeaders: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        }
      });
      return next.handle(request);
    } else {
      // En caso de que no sea el WS-1, es decir, precisa token en la cabezera:
      // Se obtiene el token
      return Observable.fromPromise(this.storage.get('sup_token')).flatMap((token) => {
        // Se modifica de forma inmutable, haciendo el clone de la petición y haciendo el set del nuevo header (token)
        request = request.clone({
          setHeaders: {
            token: `${token}`
          }
        });
        // Se pasa al siguiente interceptor de la cadena la petición modificada
        return next.handle(request);
      });
    }
  }
}
