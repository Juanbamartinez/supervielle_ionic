import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SUPERVIELLE_ENDPOINTS } from './../constants/endpoint.constant';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import 'rxjs/add/observable/fromPromise';

@Injectable()
export class HttpService {
	constructor(private http: HttpClient, private storage: Storage) {
	}

	public request(url: string, body?: object): Observable<any> {
		return Observable.fromPromise(new Promise((resolve, reject) => {
			this.http.request('POST', url, { body: body })
				.subscribe((response: any) => {
					if (response.codigo === "CERR") {
						let tokenRequest = this.reGenerateToken();
						let initRequest = this.http.request('POST', url, { body: body });

						Observable.concat(tokenRequest, initRequest).subscribe(result => {
							if (result.codigo) {
								console.log("[DEBUG] ", result);
								resolve(result);
							}
						});
					}
					else {
						console.log("[DEBUG] ", response);
						resolve(response);
					}
				}, error => {
					console.error("[ERR] ", error);
					reject(error);
				});
		}));
	}

	public reGenerateToken(): Observable<any> {
		return Observable.fromPromise(new Promise((resolve, reject) => {
			this.http.request('POST', SUPERVIELLE_ENDPOINTS.CODE_GENERATE_TOKEN, { body: { deviceId: "JCDEVICE1", nombre: "superviellemb" } })
				.subscribe((result: any) => {
					this.storage.set('sup_token', result.token)
						.then(result => {
							resolve(result);
						})
				}, error => reject(error));
		}));
	}
}
