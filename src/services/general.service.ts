import { Injectable } from '@angular/core';
import { SUPERVIELLE_ENDPOINTS } from './../constants/endpoint.constant';
import { HttpService } from './http.service';
import { Observable } from "rxjs";

@Injectable()
export class GeneralService {
  constructor(private httpService: HttpService) {
  }

  public getModuleList(_zona: any): Observable<any> {
    return this.httpService.request(SUPERVIELLE_ENDPOINTS.CODE_MODULE_LIST, { idDisp: "JBDEVICE0" })
  }

  public getBannersHP(_zona: any): Observable<any> {
    return this.httpService.request(SUPERVIELLE_ENDPOINTS.CODE_BANNER_HP, {})
  }

  public getATMAndBranchList(): Observable<any> {
    return this.httpService.request(SUPERVIELLE_ENDPOINTS.CODE_ATM_AND_BRANCH_LIST, {})
  }

  public sendNewstForm(_params: any): Observable<any> {
    return this.httpService.request(SUPERVIELLE_ENDPOINTS.CODE_NEWS_FORM, _params);
  }

  public sendContactForm(_params: any): Observable<any> {
    return this.httpService.request(SUPERVIELLE_ENDPOINTS.CODE_CONTACT_FORM, _params);
  }
}
