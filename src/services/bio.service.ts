import { Injectable } from '@angular/core';
import { SUPERVIELLE_ENDPOINTS } from './../constants/endpoint.constant';
import { HttpService } from './http.service';
import { Observable } from "rxjs";

@Injectable()
export class BioService {
  constructor(private httpService: HttpService) {
  }

  public getBioAvailable(canal: string, deviceId: string){
    return this.httpService.request(SUPERVIELLE_ENDPOINTS.CODE_BIO_AVAILABLE, { canal: canal, idDispositivo: deviceId });
  }
}
