import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { HttpService } from './http.service';
import { NetworkService } from './network.service';
import { GeneralService } from './general.service';
import { NewsService } from './news.service';
import { BenefitService } from './benefit.service';
import { HelpService } from './help.service';
import { TermsService } from './terms.service';
import { TokenInterceptor } from './token.interceptor';
import { BioService } from './bio.service';
// FIXME?
import { AlertService } from './alert.service';
import { UtilService } from './util.service';

@NgModule({
  declarations: [],
  imports: [
      IonicModule,
      CommonModule
  ],
  exports: [],
  providers: [
      HttpService,
      NetworkService,
      GeneralService,
      NewsService,
      BenefitService,
      AlertService,
      HelpService,
      TermsService,
      TokenInterceptor,
      UtilService,
      BioService
  ]
})
export class ServicesModule { }
