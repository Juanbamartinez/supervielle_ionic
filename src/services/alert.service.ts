import { Injectable } from '@angular/core';
import { AlertController, Alert } from 'ionic-angular';

@Injectable()
export class AlertService {
  constructor(public alertCtrl: AlertController) {
  }

  public showAlert(_title?:string, _text?:string, _buttons?:string[]): Alert {
    let alert = this.alertCtrl.create({
      title: _title,
      subTitle: _text,
      buttons: _buttons
    });
    return alert;
  }

}
