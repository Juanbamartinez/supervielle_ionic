import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { Subscription } from 'rxjs/Subscription';
import { Platform, AlertController } from 'ionic-angular';

@Injectable()
export class NetworkService {
  public disconnectNetwork: Subscription;
  constructor(private network: Network, private platform: Platform, private alertCtrl: AlertController) {
    platform.ready()
      .then(() => {
        this.disconnectNetwork = network.onDisconnect().subscribe(data => {
          let alert = this.alertCtrl.create({
            title: "Supervielle",
            subTitle: "Sin conexión a Internet.",
            buttons: ['Aceptar']
          });
          alert.present();
        }, error => console.log("Error connectionNetwork: ", error))
      })
      .catch(error => console.log(error));
  }

  public isConnected(): Promise<Boolean> {
    return new Promise((resolve, reject) => {
      this.platform.ready()
        .then(() => {
          if ((this.network.type != "none") && (this.network.type != "unknown")) {
            resolve(true);
          } else {
            reject("No hay conexión a Internet");
          }
        })
        .catch(error => reject(error))
    })
  }
}
