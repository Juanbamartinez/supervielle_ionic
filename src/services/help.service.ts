import { Injectable } from '@angular/core';
import { SUPERVIELLE_ENDPOINTS } from './../constants/endpoint.constant';
import { HttpService } from './http.service';
import { Observable } from "rxjs";

@Injectable()
export class HelpService {
  constructor(private httpService: HttpService) {
  }

  public getHelpList(): Observable<any> {
    return this.httpService.request(SUPERVIELLE_ENDPOINTS.CODE_FAQ, {})
  }
}
