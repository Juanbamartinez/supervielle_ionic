import { NgModule } from '@angular/core';
import { HelpListPageModule } from './help-list/help-list.module';
import { HelpDetailPageModule } from './help-detail/help-detail.module';

@NgModule({
  declarations: [
  ],
  imports: [
    HelpListPageModule,
    HelpDetailPageModule
  ],
})
export class HelpPageModule { }
