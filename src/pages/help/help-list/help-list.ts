import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UtilService } from './../../../services/util.service';
import { AlertService } from '../../../services/alert.service';
import { HelpDetailPage } from './../help-detail/help-detail';
import { HelpService } from './../../../services/help.service';

@IonicPage()
@Component({
  selector: 'page-help-list',
  templateUrl: 'help-list.html',
})
export class HelpListPage {
  private helpList: Array<{ pregunta?: string, respuesta?: string }>;
  private loading: any;

  constructor(
    private navCtrl: NavController, private navParams: NavParams, private utilService: UtilService,
    private helpService: HelpService, private alertService: AlertService
  ) {
    this.loading = this.utilService.getLoading();
    this.helpList = [];
  }

  ionViewDidLoad() {
    this.getHelpList();
  }

  public getHelpList(): void {
    this.loading.present();
    this.helpService.getHelpList()
      .subscribe(response => {
        this.loading.dismiss();
        if (response.codigo === "OK") {
          this.helpList = response.listaPregutasFrecuentes;
        } else {
          let alert = this.alertService.showAlert("Supervielle", response.descripcion, ['Aceptar']);
          alert.present();
        }
      }, error => {
        console.error(error);
        this.loading.dismiss();
        let alert = this.alertService.showAlert("Supervielle", error.descripcion, ['Aceptar']);
        alert.present();
      });
  }

  public goDetail(item): void {
    this.navCtrl.push(HelpDetailPage, {
      data: item
    });
  }

}
