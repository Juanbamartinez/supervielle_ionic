import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../../components/components.module';
import { HelpDetailPage } from './help-detail';

@NgModule({
  declarations: [
    HelpDetailPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(HelpDetailPage),
  ],
})
export class HelpDetailPageModule { }
