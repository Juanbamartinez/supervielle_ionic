import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-help-detail',
  templateUrl: 'help-detail.html',
})
export class HelpDetailPage {
  private helpItem: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.helpItem = [];
    this.helpItem = this.navParams.get('data');
  }
}
