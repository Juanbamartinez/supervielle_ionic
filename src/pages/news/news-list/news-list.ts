import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { NewsService } from './../../../services/news.service';
import { UtilService } from './../../../services/util.service';
import { AlertService } from './../../../services/alert.service';
import { NewsDetailPage } from './../news-detail/news-detail';

@IonicPage()
@Component({
  selector: 'news-list-page',
  templateUrl: 'news-list.html',
})
export class NewsListPage {
  private newsList: any[];
  private loading: any;

  constructor(
    private navCtrl: NavController, private navParams: NavParams, private newsService: NewsService,
    private utilService: UtilService, private alertService: AlertService
  ) {
    this.newsList = [];
    this.loading = this.utilService.getLoading();
  }

  ionViewDidLoad() {
    this.getNews();
  }

  public getNews(): void {
    this.loading.present();
    this.newsService.getNewsList(null)
      .subscribe(response => {
        this.loading.dismiss();
        if (response.codigo === "OK") {
          this.newsList = response.listaNovedades;
        } else {
          let alert = this.alertService.showAlert("Supervielle", response.descripcion, ['Aceptar']);
          alert.present();
        }
      }, error => {
        this.loading.dismiss();
        let alert = this.alertService.showAlert("Supervielle", error.descripcion, ['Aceptar']);
        alert.present();
      });
  }

  public goDetail(item): void {
    this.navCtrl.push(NewsDetailPage, {
      data: item
    });
    console.log("BTN_NEWS_DETAIL");
  }
}
