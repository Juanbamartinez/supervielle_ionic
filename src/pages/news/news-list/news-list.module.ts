import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../../components/components.module';
import { NewsListPage } from './news-list';

@NgModule({
  declarations: [
    NewsListPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(NewsListPage),
  ],
})
export class NewsListPageModule { }
