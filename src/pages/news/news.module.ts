import { NgModule } from '@angular/core';
import { NewsListPageModule } from './news-list/news-list.module';
import { NewsDetailPageModule } from './news-detail/news-detail.module';

@NgModule({
  declarations: [],
  imports: [
    NewsListPageModule,
    NewsDetailPageModule
  ],
})
export class NewsPageModule { }
