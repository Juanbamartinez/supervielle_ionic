import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UtilService } from './../../../services/util.service';
import { AlertService } from '../../../services/alert.service';
import { NewsService } from '../../../services/news.service';

@IonicPage()
@Component({
  selector: 'news-detail-page',
  templateUrl: 'news-detail.html',
})
export class NewsDetailPage {
  private data: any;
  private newsItem: any;
  private loading: any;

  constructor(
    private navCtrl: NavController, private navParams: NavParams, private utilService: UtilService,
    private newsService: NewsService, private alertService: AlertService
  ) {
    this.loading = this.utilService.getLoading();
    this.data = this.navParams.get('data');
    this.newsItem = {};
  }

  ionViewDidLoad() {
    this.getDetail(this.data.hashNovedad);
  }

  public getDetail(hashNovedad: string): void {
    this.loading.present();
    this.newsService.getNewsDetail(hashNovedad)
      .subscribe(response => {
        this.loading.dismiss();
        if (response.codigo === "OK") {
          this.newsItem = response;
        } else {
          let alert = this.alertService.showAlert("Supervielle", response.descripcion, ['Aceptar']);
          alert.present();
        }
      }, error => {
        this.loading.dismiss();
        let alert = this.alertService.showAlert("Supervielle", error.descripcion, ['Aceptar']);
        alert.present();
      });
  }
}
