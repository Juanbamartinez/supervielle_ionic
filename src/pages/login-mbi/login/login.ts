import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SUPERVIELLE_MESSAGES } from './../../../constants/messages.constant';
import { SUPERVIELLE_MODULES } from './../../../constants/modules.constant';
import { SUPERVIELLE_ERROR } from './../../../constants/errors.constant';
import { Storage } from '@ionic/storage';
import { UtilService } from './../../../services/util.service';
import { AlertService } from './../../../services/alert.service';
import { BioService } from './../../../services/bio.service';

@IonicPage()
@Component({
  selector: 'login-page',
  templateUrl: 'login.html',
})
export class LoginPage {
  private loginMbiForm: FormGroup;
  private userLockedBio: boolean;
  private bioFacialImages: any;
  private userErrorBio: boolean;
  private deviceBioInfo: any;
  private bioUrl: string;
  private hash_random: string;
  private hbidata: any;

  constructor(
    private navCtrl: NavController, private navParams: NavParams, private alertService: AlertService,
    private storage: Storage, private util: UtilService, private bio: BioService
  ) {
    this.loginMbiForm = new FormGroup({
      nombre: new FormControl('', [Validators.required]),
      contraseña: new FormControl('', [Validators.required])
    });
    this.userLockedBio = false;
    this.userErrorBio = false;
    this.hash_random = Math.random().toString(36).substring(7);
    this.storage.get('BIO_FACIAL_IMAGES_FUNC_MBI')
      .then((images) => {
        this.bioFacialImages = images;
      })
      .catch((err) => {
        console.error(err);
      })
  }

  ionViewDidLoad() {
    this.getDeviceBioInfo();
  }

  private goSignInProblem(): void {
    console.log("BTN_SIGING_PROBLEM");
  }

  private goRegistrationStep1(): void {
    console.log("BTN_REGISTRATION");
  }

  private doLogin(): void {
    console.log("BTN_LOGIN");
  }

  private showHelp(): void {
    let alert = this.alertService.showAlert("Clave Única", SUPERVIELLE_MESSAGES.KEY_HELP_LOGIN, ['Aceptar']);
    alert.present();
    console.log("BTN_AYUDA");
  }

  private getDeviceBioInfo(): void {
    this.util.getDataModuleList(SUPERVIELLE_MODULES.module.biometria, "GetDeviceBioInfo")
      .then((data) => {
        this.deviceBioInfo = eval(data.estado);
        this.storage.set('BIO_FACIAL_DEVICE_INFO_FUNC', this.deviceBioInfo);
        if (this.deviceBioInfo) {
          this.checkBioAvailability();
        }
      })
      .catch((err) => {
        console.error(err);
      })
  }

  private checkBioAvailability(): void {
    this.bio.getBioAvailable("MBI", this.util.getDeviceInfo().uuid)
      .subscribe(response => {
        if (response.codigo === SUPERVIELLE_ERROR.ok) {
          console.log("Se debería abrir el SDK.");
          this.storage.set("BIO_DEVICE_INFO_CODE", response.codigo);
        } else if (response.codigo === SUPERVIELLE_ERROR.unknowDevice) {
          this.bioUrl = this.bioFacialImages.urls[0] + "?" + this.hash_random.toString();
          this.storage.set("BIO_DEVICE_INFO_CODE", response.codigo);
        } else if (response.codigo === SUPERVIELLE_ERROR.bioLocked) {
          this.storage.set("BIO_DEVICE_INFO_CODE", response.codigo);
        } else {
          this.bioUrl = undefined;
        }
      }, err => {
        console.error("[ERROR] Error en obtener la información de Biometría: ", err);
        this.bioUrl = undefined;
      });
  }

}
