import { NgModule } from '@angular/core';
import { LoginPageModule } from './login/login.module';

@NgModule({
  declarations: [],
  imports: [
    LoginPageModule
  ],
})
export class LoginMbiPageModule { }
