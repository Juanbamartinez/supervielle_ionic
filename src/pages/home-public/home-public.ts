import { Component } from '@angular/core';
import { IonicPage, NavController, Events, LoadingController } from 'ionic-angular';
import { GeneralService } from '../../services/general.service';
import { Storage } from '@ionic/storage';
import { SUPERVIELLE_CONST } from './../../constants/supervielle.constant';
import { SUPERVIELLE_ENDPOINTS } from './../../constants/endpoint.constant';
import { SUPERVIELLE_MODULES } from './../../constants/modules.constant';
import { HttpService } from './../../services/http.service';
import { UtilService } from './../../services/util.service';

// FIXME
import { AtmAndBranchPage } from './../../pages/localization/atm-and-branch/atm-and-branch';
import { ContactCenterListPage } from './../../pages/contact/contact-center-list/contact-center-list';
import { BenefitListPage } from './../../pages/benefit/benefit-list/benefit-list';
import { BannerDetailPage } from './../../pages/banner-detail/banner-detail';
import { LoginPage } from './../../pages/login-mbi/login/login';

@IonicPage()
@Component({
  selector: 'page-home-public',
  templateUrl: 'home-public.html'
})
export class HomePublicPage {
  private srcStaticImg?: { urlBanner?: string };
  private listaBannerInferior?: any;
  private loading: any;
  private isConnected: boolean;
  private bioFacialImages?: any;

  constructor(private events: Events, private generalService: GeneralService, private storage: Storage, private navCtrl: NavController,
    private httpService: HttpService, private utilService: UtilService
  ) {
    events.publish(SUPERVIELLE_CONST.MENU.EVENT, SUPERVIELLE_CONST.MENU.PUBLIC);
    this.listaBannerInferior = [];
    this.srcStaticImg = {};
    this.bioFacialImages = {
      urls: []
    };
    this.loading = this.utilService.getLoading();
    this.initFronWS();
  }

  public initFronWS(): void {
    this.loading.present();
    this.httpService.request(SUPERVIELLE_ENDPOINTS.CODE_GENERATE_TOKEN, { deviceId: "JBDEVICE0", nombre: "superviellemb" })
      .subscribe(response => {
        if (response) {
          this.storage.set('sup_token', response.token)
            .then(response => {
              this.getCurrentPosition();
              this.getModuleList();
              this.getBannersHP();
              this.loadATMBrachList();
            })
            .catch(error => console.error(error));
          this.isConnected = true;
        }
      }, error => {
        this.isConnected = false;
        this.loading.dismiss();
        console.error(error);
      });
  }

  public getModuleList(): void {
    this.generalService.getModuleList(null)
      .subscribe(response => {
        this.storage.set('moduleList', response.listaModulos);
        this.getBioFacialImages();
      }, error => {
        this.loading.dismiss();
        console.error(error);
      });
  }

  public getBannersHP(): void {
    this.generalService.getBannersHP(null)
      .subscribe(response => {
        if (response.codigo != "OK") {
          console.error("[ERROR] Obtener banners: ", response);
        } else {
          this.listaBannerInferior = response.listaBannerInferior;
          this.srcStaticImg = response.bannerSuperior;
        }
        this.isConnected = true;
        this.loading.dismiss();
      }, error => {
        this.listaBannerInferior = [
          { urlBanner: "assets/imgs/banner_sin_conexion_01.jpg" },
          { urlBanner: "assets/imgs/banner_sin_conexion_02.jpg" },
          { urlBanner: "assets/imgs/banner_sin_conexion_03.jpg" }
        ];
        this.srcStaticImg = { urlBanner: "./assets/imgs/banner_sin_conexion.png" };
        this.isConnected = false;
        this.loading.dismiss();
        console.error(error);
      });
  }

  public loadATMBrachList(): void {
    this.storage.get('branchListUpdated')
      .then((date) => {
        let lastUpdate = date;
        let now = new Date().getTime();
        let week = 7 * 24 * 60 * 60 * 1000;
        if (!lastUpdate || (now - lastUpdate) > week) {
          this.generalService.getATMAndBranchList()
            .subscribe(response => {
              this.storage.set('brachList', response.redes ? response.redes : []);
              this.storage.set('branchListUpdated', new Date().getTime());
            }, error => {
              console.error(error);
            });
        }
      })
      .catch((err) => {
        console.error(err);
      });
  }

  private getBioFacialImages(): void {
    this.utilService.getDataModuleList(SUPERVIELLE_MODULES.module.biometria, "SetEnrol")
      .then((images) => {
        if (images.listaParametro[0].valor) {
          this.extractImages(images.listaParametro[0].valor);
        }
      })
      .catch((err) => {
        console.error(err);
      });
  }

  private extractImages(bioImgs: any): void {
    let imgs = bioImgs;
    let imgsAttr = [];
    for (let img in imgs){
      imgsAttr.push(img);
    }
    for (let i=0; i<imgsAttr.length; i++){
      this.bioFacialImages.urls.push(imgs[imgsAttr[i]]);
    }
    this.storage.set('BIO_FACIAL_IMAGES_FUNC_MBI', this.bioFacialImages);
  }

  private getCurrentPosition(): void {
    this.utilService.getCurrentPosition()
      .then((pos) => {
        this.storage.set('position', { latitude: pos.coords.latitude, longitude: pos.coords.longitude })
          .then((result) => { console.log("Posición: ", result) })
          .catch((error) => { console.error("Posición error: ", error) });
      })
      .catch((err) => {
        console.error(err);
      });
  }

  public goATMAndBranch(): void {
    this.navCtrl.push(AtmAndBranchPage);
    console.log("BTN_CAJEROS_SUCURSALES_LOGGER");
  }

  public goContactCenter(): void {
    this.navCtrl.push(ContactCenterListPage);
    console.log("BTN_CENTRO_CONTACTOS_LOGGER");
  }

  public goSearchType(): void {
    this.navCtrl.push(BenefitListPage);
    console.log("BTN_BENEFICIOS_LOGGER");
  }

  public goBannerDetail(data, i, $event): void {
    if (this.isConnected) {
      data.index = i;
      this.navCtrl.push(BannerDetailPage, {
        data: data
      });
      console.log("BTN_BANNER_DETAIL");
    } else {
      $event.preventDefault();
    }
  }

  public goHomeBanking(): void {
    this.navCtrl.push(LoginPage);
    console.log("BTN_LOGIN_MBI");
  }
}
