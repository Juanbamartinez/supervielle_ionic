import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePublicPage } from './home-public';
import { ComponentsModule } from './../../components/components.module';

@NgModule({
  declarations: [
    HomePublicPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(HomePublicPage),
  ],
})
export class HomePublicPageModule { }
