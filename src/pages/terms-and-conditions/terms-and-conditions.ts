import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { UtilService } from './../../services/util.service';
import { TermsService } from './../../services/terms.service';

@IonicPage()
@Component({
  selector: 'page-terms-and-conditions',
  templateUrl: 'terms-and-conditions.html',
})
export class TermsAndConditionsPage {
  private termsItem: any;
  private loading: any;

  constructor(private termsService: TermsService, private utilService: UtilService) {
    this.loading = this.utilService.getLoading();
    this.termsItem = [];
  }

  ionViewDidLoad() {
    this.getTerms();
  }

  public getTerms(): void {
    this.loading.present();
    this.termsService.getTermsAndConditions()
      .subscribe(response => {
        this.termsItem = response;
        if (response) {
          this.loading.dismiss();
        }
      }, error => {
        console.error(error);
        this.loading.dismiss();
      });
  }

}
