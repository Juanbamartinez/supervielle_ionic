import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../components/components.module';
import { TermsAndConditionsPage } from './terms-and-conditions';

@NgModule({
  declarations: [
    TermsAndConditionsPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(TermsAndConditionsPage),
  ],
})
export class TermsAndConditionsPageModule { }
