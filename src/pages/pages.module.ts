import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from "@angular/common";

import { LocalizationPageModule } from './localization/localization.module';
import { BannerDetailPageModule } from './banner-detail/banner-detail.module';
import { BenefitPageModule } from './benefit/benefit.module';
import { ContactPageModule } from './contact/contact.module';
import { HomePublicPageModule } from './home-public/home-public.module';
import { LoginBusinessPageModule } from './login-business/login-business.module';
import { NewsPageModule } from './news/news.module';
import { SearchTypePageModule } from './search-type/search-type.module';
import { HelpPageModule } from './help/help.module';
import { TermsAndConditionsPageModule } from './terms-and-conditions/terms-and-conditions.module';
import { LoginMbiPageModule } from './login-mbi/login-mbi.module';

import { ComponentsModule } from './../components/components.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    LocalizationPageModule,
    BannerDetailPageModule,
    BenefitPageModule,
    ContactPageModule,
    HomePublicPageModule,
    LoginBusinessPageModule,
    NewsPageModule,
    SearchTypePageModule,
    HelpPageModule,
    TermsAndConditionsPageModule,
    LoginMbiPageModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PagesModule { }
