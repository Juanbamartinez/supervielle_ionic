import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../../components/components.module';
import { BenefitListPage } from './benefit-list';

@NgModule({
  declarations: [
    BenefitListPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(BenefitListPage),
  ],
})
export class BenefitListPageModule { }
