import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Benefit } from './../../../models/benefit.model';
import { BenefitService } from './../../../services/benefit.service';
import { BenefitDetailPage } from '../benefit-detail/benefit-detail';
import { MapMarkerPage } from './../../localization/map-marker/map-marker';
import { Storage } from '@ionic/storage';
import { UtilService } from './../../../services/util.service';
import { AlertService } from '../../../services/alert.service';

@IonicPage()
@Component({
  selector: 'page-benefit-list',
  templateUrl: 'benefit-list.html',
})
export class BenefitListPage {
  private benefitList: Benefit[];
  private data: any[];
  private loading: any;
  private origin_lat_lng: {};
  private max_results: number;

  constructor(
    private navCtrl: NavController, private navParams: NavParams, private benefitService: BenefitService,
    private utilService: UtilService, private storage: Storage, private alertService: AlertService
  ) {
    this.getCurrentPosition();
    this.loading = this.utilService.getLoading();
    this.benefitList = [];
    this.max_results = 0;
  }

  ionViewDidLoad() {
    this.getBenefit();
  }

  public getBenefit(): void {
    this.loading.present();
    //FIX-ME  Obtener zona
    this.benefitService.getBenefitList('Buenos Aires')
      .subscribe(response => {
        if (response.codigo === "OK") {
          this.sortBenefit(response.listaBeneficios);
        } else {
          let alert = this.alertService.showAlert("Supervielle", response.descripcion, ['Aceptar']);
          alert.present();
        }
      }, error => {
        this.loading.dismiss();
        let alert = this.alertService.showAlert("Supervielle", error.descripcion, ['Aceptar']);
        alert.present();
      });
  }

  public nextPage(): void {
    this.loading = this.utilService.getLoading();
    this.loading.present();
    this.max_results = Math.min(this.benefitList.length, this.max_results + 10);
    this.loading.dismiss();
  }

  public goDetail(item: Benefit): void {
    let list = [];
    list.push(item);
    this.navCtrl.push(BenefitDetailPage, {
      data: list,
      origin: this.origin_lat_lng
    });
    console.log("BTN_BENEFITS_DETAIL");
  }

  public goToMap(): void {
    this.navCtrl.push(MapMarkerPage, {
      data: this.benefitList.slice(0, this.max_results),
      origin: this.origin_lat_lng,
      title: "Beneficios"
    });
  }

  private sortBenefit(list): void {
    let benefitList = list;
    let aux = [];
    for (let i = 0; i < benefitList.length; i++) {
      let benefit = benefitList[i];
      benefit.listaLocales.forEach(function(local) {
        let LatLgn = local.coordenadas.split(',');
        let lat = parseFloat(LatLgn[0]);
        let lng = parseFloat(LatLgn[1]);
        let vigencia = benefit.vigencia.split(" - ");
        let vigenciaStart = vigencia[0];
        let vigenciaEnd = vigencia[1];
        let b: Benefit = new Benefit();
        b.rubro = benefit.rubro;
        b.marca = benefit.marca;
        b.logo = "./assets/imgs/beneficio-sin-foto.png"; // FIXME: Colocar el logo que viene en el objeto
        b.descuentoCuotas = benefit.descuentoCuotas;
        b.tarjeta = benefit.tarjeta;
        b.direccion = local.direccion;
        b.lat = lat;
        b.lng = lng;
        b.distance = 0;
        b.identite = benefit.identite;
        b.raw_item = benefit;
        b.nombre = "BENEFICIO - " + benefit.marca;
        b.vigencia = {
          desde: vigenciaStart,
          hasta: vigenciaEnd
        }
        b.idBeneficio = benefit.idBeneficio;
        aux.push(b);
      });
    }
    this.benefitList = aux;
    this.prepareData();
  }

  private prepareData(): void {
    this.setDistance(this.benefitList, this.origin_lat_lng);
    this.max_results = Math.min(this.benefitList.length, 10);
    this.loading.dismiss();
  };

  private setDistance(list: Benefit[], pos): void {
    if (pos) {
      for (let i = 0; i < list.length; i++) {
        list[i].distance = parseFloat(Number(this.utilService.getDistanceFromLatLngInKm(list[i].lat, list[i].lng, pos.latitude, pos.longitude)).toFixed(2));
      }
    }
  }

  private getCurrentPosition(): void {
    this.storage.get('position')
      .then((pos) => {
        this.origin_lat_lng = {
          latitude: pos.latitude,
          longitude: pos.longitude
        }
      }).catch((err) => {
        console.error("getCurrentPosition error: ", err);
      });
  }

}
