import { NgModule } from '@angular/core';
import { BenefitListPageModule } from './benefit-list/benefit-list.module';
import { BenefitDetailPageModule } from './benefit-detail/benefit-detail.module';

@NgModule({
  declarations: [
  ],
  imports: [
    BenefitListPageModule,
    BenefitDetailPageModule
  ],
})
export class BenefitPageModule { }
