import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../../components/components.module';
import { BenefitDetailPage } from './benefit-detail';

@NgModule({
  declarations: [
    BenefitDetailPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(BenefitDetailPage),
  ],
})
export class BenefitDetailPageModule { }
