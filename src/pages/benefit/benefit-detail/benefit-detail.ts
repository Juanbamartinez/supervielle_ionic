import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Benefit } from './../../../models/benefit.model';
import { BenefitService } from './../../../services/benefit.service';
import { MapMarkerPage } from './../../localization/map-marker/map-marker';
import { UtilService } from './../../../services/util.service';
import { AlertService } from '../../../services/alert.service';

@IonicPage()
@Component({
  selector: 'page-benefit-detail',
  templateUrl: 'benefit-detail.html',
})
export class BenefitDetailPage {
  private data: Benefit[];
  private origin: any;
  private loading: any;

  constructor(
    private navCtrl: NavController, private navParams: NavParams, private benefitService: BenefitService,
    private utilService: UtilService, private alertService: AlertService
  ) {
    this.loading = this.utilService.getLoading();
    this.origin = this.navParams.get('origin');
    this.data = this.navParams.get('data');
    this.getBenefitDetail(this.data[0].idBeneficio);
  }

  public getBenefitDetail(id: string): void {
    this.loading.present();
    this.benefitService.getBenefitDetail(id)
      .subscribe(response => {
        this.loading.dismiss();
        if (response.codigo === "OK") {
          this.data[0].legales = response.legales;
          this.data[0].paginaWeb = response.paginaWeb;
        } else {
          let alert = this.alertService.showAlert("Supervielle", response.descripcion, ['Aceptar']);
          alert.present();
        }
      }, error => {
        console.error(error);
        this.loading.dismiss();
      });
  }

  private goToMap(): void {
    this.navCtrl.push(MapMarkerPage, {
      data: this.data,
      origin: this.origin,
      title: "Beneficios"
    });
  }
}
