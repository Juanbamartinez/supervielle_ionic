import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AtmAndBranchPage } from '../../pages/localization/atm-and-branch/atm-and-branch';

@IonicPage()
@Component({
  selector: 'page-search-type',
  templateUrl: 'search-type.html',
})
export class SearchTypePage {

  constructor(private navCtrl: NavController, private navParams: NavParams) {
  }

  public show(): void {
    this.navCtrl.push(AtmAndBranchPage)
  }

}
