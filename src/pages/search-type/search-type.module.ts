import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../components/components.module';
import { SearchTypePage } from './search-type';

@NgModule({
  declarations: [
    SearchTypePage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SearchTypePage),
  ],
})
export class SearchTypePageModule { }
