import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UtilService } from './../../../services/util.service';
import { AgmCoreModule, AgmInfoWindow } from '@agm/core';

@IonicPage()
@Component({
  selector: 'map-marker',
  templateUrl: 'map-marker.html',
})
export class MapMarkerPage {
  private data: any[];
  private title: string;
  private loading: any;
  private origin: any;
  private centerCoords: any;
  private marker: any;

  constructor(
    private navCtrl: NavController, private navParams: NavParams, private utilService: UtilService
  ) {
    this.loading = this.utilService.getLoading();
    this.data = this.navParams.get('data');
    this.origin = this.navParams.get('origin');
    this.title = this.navParams.get('title');
    this.centerCoords = {
      latitude: this.origin.latitude,
      longitude: this.origin.longitude
    }
    this.loading.present();
  }

  ionViewDidLoad() {
    this.loading.dismiss();
  }

  // FIXME buscar los tipos de gmap e infoWindow (no están en el core de AgmCoreModule)
  private showMarkerInfo(marker: any, infoWindowOpen: any, gmap: any): void {
    if (gmap.lastOpen) {
      gmap.lastOpen.close();
    }
    gmap.lastOpen = infoWindowOpen;
    this.centerCoords.latitude = marker.lat;
    this.centerCoords.longitude = marker.lng;
  }

  private originMarker(gmap: any): void {
    if (gmap.lastOpen) {
      gmap.lastOpen.close();
    }
    this.centerCoords.latitude = this.origin.latitude;
    this.centerCoords.longitude = this.origin.longitude;
  }
}
