import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../../components/components.module';
import { MapMarkerPage } from './map-marker';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    MapMarkerPage,
  ],
  imports: [
    ComponentsModule,
    AgmCoreModule,
    IonicPageModule.forChild(MapMarkerPage),
  ],
})
export class MapMarkerPageModule { }
