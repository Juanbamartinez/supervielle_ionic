import { NgModule } from '@angular/core';
import { AtmAndBranchPageModule } from './atm-and-branch/atm-and-branch.module';
import { AtmAndBranchListPageModule } from './atm-and-branch-list/atm-and-branch-list.module';
import { AtmAndBranchDetailPageModule } from './atm-and-branch-detail/atm-and-branch-detail.module';
import { MapMarkerPageModule } from './map-marker/map-marker.module';

@NgModule({
  declarations: [],
  imports: [
    AtmAndBranchListPageModule,
    AtmAndBranchPageModule,
    AtmAndBranchDetailPageModule,
    MapMarkerPageModule
  ],
})
export class LocalizationPageModule { }
