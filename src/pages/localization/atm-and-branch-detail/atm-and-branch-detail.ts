import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UtilService } from './../../../services/util.service';
import { MapMarkerPage } from './../map-marker/map-marker';

@IonicPage()
@Component({
  selector: 'page-atm-and-branch-detail',
  templateUrl: 'atm-and-branch-detail.html',
})
export class AtmAndBranchDetailPage {
  private data: any;
  private origin: any;
  private title: string;
  private loading: any;

  constructor(private navCtrl: NavController, private navParams: NavParams, private utilService: UtilService) {
    this.loading = this.utilService.getLoading();
    this.origin = this.navParams.get('origin');
    this.data = this.navParams.get('data');
    this.title = this.data[0].raw_data.title;
    this.loading.present();
  }

  ionViewDidLoad() {
    this.loading.dismiss();
  }

  private goToMap(): void {
    this.navCtrl.push(MapMarkerPage, {
      data: this.data,
      origin: this.origin,
      title: this.title
    });
  }
}
