import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../../components/components.module';
import { AtmAndBranchDetailPage } from './atm-and-branch-detail';

@NgModule({
  declarations: [
    AtmAndBranchDetailPage
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(AtmAndBranchDetailPage)
  ],
})
export class AtmAndBranchDetailPageModule { }
