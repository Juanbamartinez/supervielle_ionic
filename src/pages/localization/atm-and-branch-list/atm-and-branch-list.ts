import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { UtilService } from './../../../services/util.service';
import { AtmAndBranchDetailPage } from './../atm-and-branch-detail/atm-and-branch-detail';
import { MapMarkerPage } from './../map-marker/map-marker';

@IonicPage()
@Component({
  selector: 'page-atm-and-branch-list',
  templateUrl: 'atm-and-branch-list.html',
})
export class AtmAndBranchListPage {
  private data: any;
  private title: string;
  private list: any[];
  private loading: any;
  private origin_lat_lng: {};
  private max_results: number;

  constructor(
    private navCtrl: NavController, private navParams: NavParams, private utilService: UtilService,
    private storage: Storage
  ) {
    this.loading = this.utilService.getLoading();
    this.data = this.navParams.get('data');
    this.title = this.data.title;
    this.list = [];
    this.max_results = 0;
    this.getCurrentPosition();
  }

  ionViewDidLoad() {
    this.loading.present();
    this.getList();
  }

  public getList(): void {
    this.storage.get('brachList')
      .then((data) => {
        this.buildATMAndBranchList(data);
      })
      .catch((err) => {
        console.error(err);
        this.loading.dismiss();
      });
  }

  public goDetail(item): void {
    item.raw_data = this.data;
    let list = [];
    list.push(item);
    this.navCtrl.push(AtmAndBranchDetailPage, {
      data: list,
      origin: this.origin_lat_lng
    });
  }

  public goToMap(): void {
    this.navCtrl.push(MapMarkerPage, {
      data: this.list.slice(0, this.max_results),
      origin: this.origin_lat_lng,
      title: this.data.title
    });
  }

  public nextPage(): void {
    this.loading = this.utilService.getLoading();
    this.loading.present();
    this.max_results = Math.min(this.list.length, this.max_results + 10);
    this.loading.dismiss();
  }

  private buildATMAndBranchList(data): void {
    let aux = [];
    for (let i = 0; i < data.length; i++) {
      let item = data[i];
      let LatLgn = item.coordenadas.split(',');
      let lat = LatLgn[0];
      let lng = LatLgn[1];
      let IMG_ATM, PIN_ATM;
      IMG_ATM = "./assets/imgs/sucursales-pic.png";
      PIN_ATM = "./assets/imgs/pin_supervielle.png";
      if (item.tipo === 'CAJERO') {
        if (item.banco === 'Banelco') {
          IMG_ATM = "./assets/imgs/banelco.jpg";
          PIN_ATM = "./assets/imgs/pin_banelco.png";
        } else if (item.banco === 'Link') {
          IMG_ATM = "./assets/imgs/link.jpg";
          PIN_ATM = "./assets/imgs/pin_link.png";
        }
      }
      aux.push({
        logo: IMG_ATM,
        tipo: item.tipo,
        nombre: item.nombre,
        direccion: item.direccion,
        lat: parseFloat(lat),
        lng: parseFloat(lng),
        numero: item.numero,
        icon_pin: PIN_ATM,
        raw_item: item
      });
    }
    this.filterList(aux, this.data.name);
  }

  private filterList(aux_list, type): void {
    let result = [];
    function isValid(item) {
      if (item.tipo != type) {
        return false;
      }
      return true;
    }
    aux_list.forEach(function(item) {
      if (isValid(item)) {
        result.push(item);
      }
    });
    this.list = result;
    this.prepareData();
  }

  private prepareData(): void {
    this.max_results = Math.min(this.list.length, 10);
    this.loading.dismiss();
  };

  private getCurrentPosition(): void {
    this.storage.get('position')
      .then((pos) => {
        this.origin_lat_lng = {
          latitude: pos.latitude,
          longitude: pos.longitude
        }
      }).catch((err) => {
        console.error("getCurrentPosition error: ", err);
      });
  }
}
