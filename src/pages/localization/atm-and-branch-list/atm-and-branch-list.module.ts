import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../../components/components.module';
import { AtmAndBranchListPage } from './atm-and-branch-list';

@NgModule({
  declarations: [
    AtmAndBranchListPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(AtmAndBranchListPage),
  ],
})
export class AtmAndBranchListPageModule { }
