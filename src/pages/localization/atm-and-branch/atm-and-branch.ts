import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AtmAndBranchListPage } from './../atm-and-branch-list/atm-and-branch-list';
import { SUPERVIELLE_CONST } from './../../../constants/supervielle.constant';

@IonicPage()
@Component({
  selector: 'page-atm-and-branch',
  templateUrl: 'atm-and-branch.html',
})
export class AtmAndBranchPage {
  private ATM_AND_BRANCH_ITEMS: any;
  constructor(public navCtrl: NavController) {
    this.ATM_AND_BRANCH_ITEMS = SUPERVIELLE_CONST.ATM_AND_BRANCH_ITEMS;
  }

  public searchType(branch_list_item: any): void {
    this.navCtrl.push(AtmAndBranchListPage, {
      data: branch_list_item
    });
  }

}
