import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../../components/components.module';
import { AtmAndBranchPage } from './atm-and-branch';

@NgModule({
  declarations: [
    AtmAndBranchPage
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(AtmAndBranchPage)
  ],
})
export class AtmAndBranchPageModule { }
