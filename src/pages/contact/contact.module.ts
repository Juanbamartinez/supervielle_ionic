import { NgModule } from '@angular/core';
import { ContactCenterListPageModule } from './contact-center-list/contact-center-list.module';
import { ContactCenterDetailPageModule } from './contact-center-detail/contact-center-detail.module';

@NgModule({
  declarations: [
  ],
  imports: [
    ContactCenterListPageModule,
    ContactCenterDetailPageModule
  ],
})
export class ContactPageModule { }
