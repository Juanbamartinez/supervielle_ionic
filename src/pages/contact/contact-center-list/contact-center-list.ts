import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ContactCenterDetailPage } from '../contact-center-detail/contact-center-detail';
import { CONST } from '../../../constants/settings.constant'

@IonicPage()
@Component({
  selector: 'page-contact-center-list',
  templateUrl: 'contact-center-list.html',
})
export class ContactCenterListPage {
  private contactCenters: Array<{ id?: string, description?: string, type?: string, detail?: string }>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.contactCenters = [];
    CONST.setting.contactCenterList.contactCenterItem.forEach(item => this.contactCenters.push(item));
  }

  public itemSelected(event, item): void {
    this.navCtrl.push(ContactCenterDetailPage, {
      item: item
    });
  }

}
