import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../../components/components.module';
import { ContactCenterListPage } from './contact-center-list';

@NgModule({
  declarations: [
    ContactCenterListPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ContactCenterListPage),
  ],
})
export class ContactCenterListPageModule { }
