import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralService } from './../../../services/general.service';
import { UtilService } from './../../../services/util.service';
import { AlertService } from './../../../services/alert.service';

@IonicPage()
@Component({
  selector: 'page-contact-center-detail',
  templateUrl: 'contact-center-detail.html',
})
export class ContactCenterDetailPage {
  private selectedItem: any;
  private loading: any;
  public headerTitle: string;

  constructor(
    private navCtrl: NavController, private navParams: NavParams, private generalService: GeneralService,
    private alertService: AlertService, private utilService: UtilService
  ) {
    this.loading = this.utilService.getLoading();
    this.selectedItem = navParams.get('item');
    this.headerTitle = this.selectedItem.type === "1" ? "Form. de Contacto" : "Centro de Contacto";
  }

  private goFacebook(): void {
    window.open('https://facebook.com/BancoSupervielle', '_system', 'location=yes');
    console.log('Test');
  }

  private goTwitter(): void {
    window.open('https://twitter.com/VosySupervielle', '_system', 'location=yes');
  }

  public onSubmit(form: any): void {
    this.loading = this.utilService.getLoading();
    this.loading.present();
    this.sendContactForm(form);
  }

  private sendContactForm(form: any): void {
    let params = {
      nombre: form.nombre,
      apellido: form.apellido,
      tipo: form.documentType,
      numero: form.documento,
      motivo: form.motivo,
      esCliente: form.cliente ? "Si" : "No",
      consulta: form.consulta,
      email: form.email,
      telefono: form.telefono
    };
    this.generalService.sendContactForm(params)
      .subscribe(response => {
        this.loading.dismiss();
        let alert = this.alertService.showAlert("Supervielle", response.descripcion, ['Aceptar']);
        alert.present();
      }, error => {
        this.loading.dismiss();
        let alert = this.alertService.showAlert("Supervielle", error.descripcion, ['Aceptar']);
        alert.present();
      });
  }

}
