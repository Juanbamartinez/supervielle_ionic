import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../../components/components.module';
import { ContactCenterDetailPage } from './contact-center-detail';

@NgModule({
  declarations: [
    ContactCenterDetailPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ContactCenterDetailPage),
  ],
})
export class ContactCenterDetailPageModule { }
