import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GeneralService } from './../../services/general.service';
import { UtilService } from './../../services/util.service';
import { AlertService } from './../../services/alert.service';

@IonicPage(

)
@Component({
  selector: 'page-banner-detail',
  templateUrl: 'banner-detail.html',
})
export class BannerDetailPage {
  private detail: any;
  private loading: any;

  constructor(
    private navCtrl: NavController, private navParams: NavParams, private generalService: GeneralService,
    private alertService: AlertService, private utilService: UtilService
  ) {
    this.detail = this.navParams.get('data');
    this.loading = this.utilService.getLoading();
  }

  public onSubmit(form: any): void {
    this.loading = this.utilService.getLoading();
    this.loading.present();
    this.sendNewsForm(form);
  }

  private sendNewsForm(form: any): void {
    let params = {
      nombre: form.nombre,
      apellido: form.apellido,
      tipo: form.documentType,
      numero: form.documento,
      motivo: form.motivo,
      esCliente: form.cliente ? "Si" : "No",
      consulta: form.consulta,
      email: form.email,
      telefono: form.telefono
    };
    this.generalService.sendNewstForm(params)
      .subscribe(response => {
        this.loading.dismiss();
        let alert = this.alertService.showAlert("Supervielle", response.descripcion, ['Aceptar']);
        alert.present();
      }, error => {
        this.loading.dismiss();
        let alert = this.alertService.showAlert("Supervielle", error.descripcion, ['Aceptar']);
        alert.present();
      });
  }
}
