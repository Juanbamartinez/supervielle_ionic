import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../components/components.module';
import { BannerDetailPage } from './banner-detail';

@NgModule({
  declarations: [
    BannerDetailPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(BannerDetailPage),
  ],
})
export class BannerDetailPageModule { }
