import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-login-business',
  templateUrl: 'login-business.html',
})
export class LoginBusinessPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  
}
