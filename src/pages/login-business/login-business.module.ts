import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from './../../components/components.module';
import { LoginBusinessPage } from './login-business';

@NgModule({
  declarations: [
    LoginBusinessPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(LoginBusinessPage),
  ],
})
export class LoginBusinessPageModule { }
