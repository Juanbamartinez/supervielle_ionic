import { environment } from '../enviroments/enviroment';

export const SUPERVIELLE_ENDPOINTS = {
  CODE_GENERATE_TOKEN: environment.SERVICES_PATH + "1",
  CODE_GET_TC: environment.SERVICES_PATH + "9",
  CODE_CONTACT_FORM: environment.SERVICES_PATH + "37",
  CODE_ATM_AND_BRANCH_LIST: environment.SERVICES_PATH + "56",
  CODE_BENEFIT_LIST: environment.SERVICES_PATH + "57",
  CODE_FAQ: environment.SERVICES_PATH + "59",
  CODE_NEWS_LIST: environment.SERVICES_PATH + "60",
  CODE_NEWS_DETAIL: environment.SERVICES_PATH + "61",
  CODE_MODULE_LIST: environment.SERVICES_PATH + "63",
  CODE_BANNER_HP: environment.SERVICES_PATH + "141",
  CODE_BENEFIT_DETAIL: environment.SERVICES_PATH + "142",
  CODE_NEWS_FORM: environment.SERVICES_PATH + "143",
  CODE_BIO_SET_ENROL: environment.SERVICES_PATH + '158',
  CODE_BIO_AVAILABLE: environment.SERVICES_PATH + '159',
  CODE_BIO_UNLOCKED: environment.SERVICES_PATH + '160',
  CODE_BIO_LOGIN: environment.SERVICES_PATH + '161',
  CODE_BIO_SET_DELETE: environment.SERVICES_PATH + '162',
  CODE_BIO_GET_TYC: environment.SERVICES_PATH + '163'
}
