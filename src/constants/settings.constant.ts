export const CONST = {
  "setting": {
    "contactCenterListTablet": {
      "contactCenterItem": [
        {
          "id": "0",
          "description": "Robo o Extravío de Tarjeta de Crédito Visa",
          "detail": "Centro de Atención Visa<br><br>Capital y GBA: <a>011 4379 3333</a><br>Interior: <a>0810 666 3368</a><br>Exterior USA: <a>1 800 396 9665</a><br>Exterior Resto del mundo: <a>1 303 967 1098</a>",
          "type": "0"
        },
        {
          "id": "1",
          "description": "Robo o Extravío de Tarjeta de Crédito Mastercard",
          "detail": "Centro de Atención MasterCard<br><br>Capital y GBA: <a>011 4340 5700</a><br>Interior: <a>0810 999 5700</a><br>Exterior: <a>5411 4340 5656</a>",
          "type": "0"
        },
        {
          "id": "2",
          "description": "Robo o Extravío de Tarjeta de Débito Visa Electrón",
          "detail": "Centro de Atención Visa: <br><br>Capital y GBA: <a>011 4379 3333</a><br>Interior: <a>0810 666 3368</a><br>Exterior USA: <a>1 800 396 9665</a><br>Exterior Resto del mundo: <a>1 303 967 1098</a>",
          "type": "0"
        },
        {
          "id": "3",
          "description": "Inconvenientes en Home Banking Personas",
          "detail": "Banca Telefónica: <br><br>Capital y GBA: <a>4959 4959</a><br>Interior: <a href=\"08103334959\">0810 333 4959</a><br>Exterior: <a>011 4959 4959</a><br><br>De lunes a viernes 8 hs a 20 hs.",
          "type": "0"
        },
        {
          "id": "4",
          "description": "Línea Ética y Valores",
          "detail": "Comunicate al <a>0800 777 7813</a><br><br>Disponible 24hs.",
          "type": "0"
        },
        {
          "id": "5",
          "description": "Formulario de Contacto",
          "type": "1"
        },
        {
          "id": "6",
          "description": "Redes Sociales",
          "type": "2"
        }
      ]
    },
    "contactCenterList": {
      "contactCenterItem": [
        {
          "id": "0",
          "description": "Robo o Extravío de Tarjeta de Crédito Visa",
          "detail": "Centro de Atención Visa<br><br>Capital y GBA: <a href=\"tel:01143793333\">011 4379 3333</a><br>Interior: <a href=\"tel:08106663368\">0810 666 3368</a><br>Exterior USA: <a href=\"tel:18003969665\">1 800 396 9665</a><br>Exterior Resto del mundo: <a href=\"tel:13039671098\">1 303 967 1098</a>",
          "type": "0"
        },
        {
          "id": "1",
          "description": "Robo o Extravío de Tarjeta de Crédito Mastercard",
          "detail": "Centro de Atención MasterCard<br><br>Capital y GBA: <a href=\"tel:01143405700\">011 4340 5700</a><br>Interior: <a href=\"tel:08109995700\">0810 999 5700</a><br>Exterior: <a href=\"tel:541143405656\">5411 4340 5656</a>",
          "type": "0"
        },
        {
          "id": "2",
          "description": "Robo o Extravío de Tarjeta de Débito Visa Electrón",
          "detail": "Centro de Atención Visa: <br><br>Capital y GBA: <a href=\"tel:01143793333\">011 4379 3333</a><br>Interior: <a href=\"tel:08106663368\">0810 666 3368</a><br>Exterior USA: <a href=\"tel:18003969665\">1 800 396 9665</a><br>Exterior Resto del mundo: <a href=\"tel:13039671098\">1 303 967 1098</a>",
          "type": "0"
        },
        {
          "id": "3",
          "description": "Inconvenientes en Home Banking Personas",
          "detail": "Banca Telefónica: <br><br>Capital y GBA: <a href=\"tel:49594959\">4959 4959</a><br>Interior: <a href=\"tel:08103334959\">0810 333 4959</a><br>Exterior: <a href=\"tel:01149594959\">011 4959 4959</a><br><br>De lunes a viernes 8 hs a 20 hs.",
          "type": "0"
        },
        {
          "id": "4",
          "description": "Línea Ética y Valores",
          "detail": "Comunicate al <a href=\"tel:08007777813\">0800 777 7813</a><br><br>Disponible 24hs.",
          "type": "0"
        },
        {
          "id": "5",
          "description": "Formulario de Contacto",
          "type": "1"
        },
        {
          "id": "6",
          "description": "Redes Sociales",
          "type": "2"
        }
      ]
    },
    "documentTypeList": {
      "documentType": [
        {
          "cod": "4",
          "descripcion": "D.N.I."
        },
        {
          "cod": "5",
          "descripcion": "L.E."
        },
        {
          "cod": "6",
          "descripcion": "LIBRETA CIVICA"
        },
        {
          "cod": "10",
          "descripcion": "PASAPORTE"
        },
        {
          "cod": "3",
          "descripcion": "C.D.I."
        },
        {
          "cod": "8",
          "descripcion": "C.I. PAIS LIMIT."
        },
        {
          "cod": "2",
          "descripcion": "C.U.I.L."
        },
        {
          "cod": "1",
          "descripcion": "C.U.I.T."
        },
        {
          "cod": "7",
          "descripcion": "CEDULA PROV."
        },
        {
          "cod": "20",
          "descripcion": "Certif. Migración"
        },
        {
          "cod": "15",
          "descripcion": "EXPED.JUDICIAL"
        },
        {
          "cod": "55",
          "descripcion": "F.C.I."
        },
        {
          "cod": "99",
          "descripcion": "INST.FIN."
        },
        {
          "cod": "9",
          "descripcion": "P. J. EXTRANJERA"
        },
        {
          "cod": "11",
          "descripcion": "P.F. EXT RES. EXT."
        },
        {
          "cod": "95",
          "descripcion": "VUELCO"
        }
      ]
    },
    "countryList": {
      "country": [
        {
          "cod": "03",
          "description": "ALEMANIA"
        },
        {
          "cod": "164",
          "description": "ANGOLA"
        },
        {
          "cod": "190",
          "description": "ANTILLAS HOLANDESAS"
        },
        {
          "cod": "53",
          "description": "ARABIA SAUDITA"
        },
        {
          "cod": "102",
          "description": "ARGELIA"
        },
        {
          "cod": "80",
          "description": "ARGENTINA"
        },
        {
          "cod": "21",
          "description": "AUSTRALIA"
        },
        {
          "cod": "13",
          "description": "AUSTRIA"
        },
        {
          "cod": "48",
          "description": "BAHAMAS"
        },
        {
          "cod": "165",
          "description": "BANGLADESH"
        },
        {
          "cod": "06",
          "description": "BELGICA"
        },
        {
          "cod": "191",
          "description": "BERMUDAS"
        },
        {
          "cod": "41",
          "description": "BOLIVIA"
        },
        {
          "cod": "196",
          "description": "BOSNIA Y HERZEGOVINA"
        },
        {
          "cod": "12",
          "description": "BRASIL"
        },
        {
          "cod": "45",
          "description": "BULGARIA"
        },
        {
          "cod": "17",
          "description": "CANADA"
        },
        {
          "cod": "14",
          "description": "CHECOSLOVAQUIA"
        },
        {
          "cod": "11",
          "description": "CHILE"
        },
        {
          "cod": "83",
          "description": "CHINA"
        },
        {
          "cod": "112",
          "description": "CHIPRE"
        },
        {
          "cod": "54",
          "description": "COLOMBIA"
        },
        {
          "cod": "152",
          "description": "CONGO"
        },
        {
          "cod": "44",
          "description": "COREA"
        },
        {
          "cod": "55",
          "description": "COSTA RICA"
        },
        {
          "cod": "51",
          "description": "CUBA"
        },
        {
          "cod": "15",
          "description": "DINAMARCA"
        },
        {
          "cod": "76",
          "description": "DOMINICANA"
        },
        {
          "cod": "47",
          "description": "ECUADOR"
        },
        {
          "cod": "57",
          "description": "EGIPTO"
        },
        {
          "cod": "58",
          "description": "EL SALVADOR"
        },
        {
          "cod": "153",
          "description": "EMIRATOS ARABES UNIDOS"
        },
        {
          "cod": "197",
          "description": "ESLOVENIA"
        },
        {
          "cod": "09",
          "description": "ESPAÑA"
        },
        {
          "cod": "158",
          "description": "ESTADO INDEPENDIENTE DE MAURICIO"
        },
        {
          "cod": "02",
          "description": "ESTADOS UNIDOS DE AMERICA"
        },
        {
          "cod": "201",
          "description": "ESTONIA"
        },
        {
          "cod": "85",
          "description": "FILIPINAS"
        },
        {
          "cod": "37",
          "description": "FINLANDIA"
        },
        {
          "cod": "04",
          "description": "FRANCIA"
        },
        {
          "cod": "43",
          "description": "GRAN DUCADO DE LUXEMBURGO"
        },
        {
          "cod": "38",
          "description": "GRECIA"
        },
        {
          "cod": "61",
          "description": "GUATEMALA"
        },
        {
          "cod": "62",
          "description": "HAITI"
        },
        {
          "cod": "63",
          "description": "HONDURAS"
        },
        {
          "cod": "183",
          "description": "HONG KONG (REPUBLICA POPULAR CHINA)"
        },
        {
          "cod": "34",
          "description": "HUNGRIA"
        },
        {
          "cod": "20",
          "description": "INDIA"
        },
        {
          "cod": "24",
          "description": "IRLANDA"
        },
        {
          "cod": "195",
          "description": "ISLA GUERNSEY (REINO UNIDO DE GRAN BRETAÑA)"
        },
        {
          "cod": "194",
          "description": "ISLAS CAIMAN (REINO UNIDO DE GRAN BRETAÑA)"
        },
        {
          "cod": "39",
          "description": "ISRAEL"
        },
        {
          "cod": "07",
          "description": "ITALIA"
        },
        {
          "cod": "19",
          "description": "JAPON"
        },
        {
          "cod": "118",
          "description": "JORDANIA"
        },
        {
          "cod": "120",
          "description": "KUWAIT"
        },
        {
          "cod": "202",
          "description": "LETONIA"
        },
        {
          "cod": "32",
          "description": "LIBANO"
        },
        {
          "cod": "156",
          "description": "MALASIA"
        },
        {
          "cod": "126",
          "description": "MARRUECOS"
        },
        {
          "cod": "33",
          "description": "MEXICO"
        },
        {
          "cod": "67",
          "description": "NICARAGUA"
        },
        {
          "cod": "28",
          "description": "NORUEGA"
        },
        {
          "cod": "22",
          "description": "NUEVA ZELANDIA"
        },
        {
          "cod": "666",
          "description": "PAIS CREADO Y MODIFICADO"
        },
        {
          "cod": "08",
          "description": "PAISES BAJOS"
        },
        {
          "cod": "198",
          "description": "PAISES VARIOS"
        },
        {
          "cod": "42",
          "description": "PAKISTAN"
        },
        {
          "cod": "193",
          "description": "PALESTINA TERRITORIO OCUPADO"
        },
        {
          "cod": "68",
          "description": "PANAMA"
        },
        {
          "cod": "69",
          "description": "PARAGUAY"
        },
        {
          "cod": "26",
          "description": "PERU"
        },
        {
          "cod": "30",
          "description": "POLONIA"
        },
        {
          "cod": "16",
          "description": "PORTUGAL"
        },
        {
          "cod": "66",
          "description": "PUERTO RICO"
        },
        {
          "cod": "01",
          "description": "REINO UNIDO DE GRAN BRETA\\A E IRLANDA NORTE"
        },
        {
          "cod": "189",
          "description": "REPUBLICA DEMOCRATICA DEL CONGO"
        },
        {
          "cod": "31",
          "description": "RUMANIA"
        },
        {
          "cod": "136",
          "description": "SENEGAL"
        },
        {
          "cod": "182",
          "description": "SINGAPUR"
        },
        {
          "cod": "35",
          "description": "SIRIA"
        },
        {
          "cod": "88",
          "description": "SRI-LANKA"
        },
        {
          "cod": "23",
          "description": "SUDAFRICA"
        },
        {
          "cod": "27",
          "description": "SUECIA"
        },
        {
          "cod": "05",
          "description": "SUIZA"
        },
        {
          "cod": "199",
          "description": "SURINAME"
        },
        {
          "cod": "56",
          "description": "TAIWAN"
        },
        {
          "cod": "140",
          "description": "THAILANDIA"
        },
        {
          "cod": "46",
          "description": "TURQUIA"
        },
        {
          "cod": "192",
          "description": "UCRANIA"
        },
        {
          "cod": "40",
          "description": "UNION DE REPUBLICAS SOCIALISTAS SOVIETICAS"
        },
        {
          "cod": "10",
          "description": "URUGUAY"
        },
        {
          "cod": "79",
          "description": "VENEZUELA"
        },
        {
          "cod": "29",
          "description": "YUGOSLAVIA"
        }
      ]
    },
    "companyPhoneList": {
      "company": [
        {
          "id": "1",
          "description": "Claro"
        },
        {
          "id": "3",
          "description": "Movistar"
        },
        {
          "id": "5",
          "description": "Personal"
        }
      ]
    }
  }
};
