export const SUPERVIELLE_CONST = {
  MENU: {
    EVENT: "menu:opciones",
    PUBLIC: 0,
    INDIVIDUOS: 1,
    EMPRESAS: 2
  },
  ATM_AND_BRANCH_ITEMS: {
    CAJERO: {
      type: 0,
      title: "Cajeros",
      name: "CAJERO"
    },
    SUCURSAL: {
      type: 1,
      title: "Sucursales",
      name: "SUCURSAL"
    },
    CENTRO: {
      type: 2,
      title: "Centro de Servicios",
      name: "CENTRO DE SERVICIO"
    }
  }
}
