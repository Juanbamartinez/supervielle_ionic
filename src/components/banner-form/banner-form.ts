import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CONST } from '../../constants/settings.constant'

@Component({
  selector: 'banner-form',
  templateUrl: 'banner-form.html'
})
export class BannerFormComponent {
  // No usar palabras reservadas para los eventos. Ver tablad e palabras reservadas.
  @Output() submitForm: EventEmitter<any> = new EventEmitter();
  private bannerForm: FormGroup;
  private documentTypeList: Array<{ cod?: string, descripcion?: string }>;

  constructor() {
    this.documentTypeList = [];
    CONST.setting.documentTypeList.documentType.forEach(item => this.documentTypeList.push(item));
    this.bannerForm = new FormGroup({
      nombre: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.maxLength(50)]),
      apellido: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.maxLength(50)]),
      documentType: new FormControl('', [Validators.required]),
      documento: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(12)]),
      motivo: new FormControl('', [Validators.required]),
      consulta: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/)]),
      telefono: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(12)]),
      cliente: new FormControl('false')
    });
  }

  private sendForm(): void {
    this.submitForm.emit(this.bannerForm.value);
  }
}
