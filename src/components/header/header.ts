import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'supervielle-header',
  templateUrl: 'header.html'
})
export class HeaderComponent {
  public showRightButton: boolean;
  public showMenu: boolean;
  public showNews: boolean;
  public showShare: boolean;
  public titleText: string = "Supervielle";

  @Input()
  public set title(title: string) {
    this.titleText = title;
  }

  @Input()
  public set right(flag: boolean) {
    this.showRightButton = flag;
  }

  @Input()
  public set menu(flag: boolean) {
    this.showMenu = flag;
  }

  @Input()
  public set share(flag: boolean) {
    this.showShare = flag;
  }

  @Input()
  public set news(flag: boolean) {
    this.showNews = flag;
  }

  constructor(public navCtrl: NavController) {
  }

}
