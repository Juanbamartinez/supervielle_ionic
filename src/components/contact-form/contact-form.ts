import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CONST } from '../../constants/settings.constant'

@Component({
  selector: 'contact-form',
  templateUrl: 'contact-form.html'
})
export class ContactFormComponent {
  @Output() submitConsultationForm: EventEmitter<any> = new EventEmitter();
  private contactForm: FormGroup;
  private documentTypeList: Array<{ cod?: string, descripcion?: string }>;

  constructor() {
    this.documentTypeList = [];
    CONST.setting.documentTypeList.documentType.forEach(item => this.documentTypeList.push(item));

    this.contactForm = new FormGroup({
      nombre: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.maxLength(50)]),
      apellido: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.maxLength(50)]),
      documentType: new FormControl(''),
      documento: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(12)]),
      email: new FormControl('', [Validators.required, Validators.pattern(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/)]),
      telefono: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(12)]),
      cliente: new FormControl('false'),
      motivo: new FormControl('', [Validators.required]),
      consulta: new FormControl('', [Validators.required])
    });
  }

  private sendForm() {
    this.submitConsultationForm.emit(this.contactForm.value);
  }

}
