import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { ContactFormComponent } from './contact-form/contact-form';
import { BannerFormComponent } from './banner-form/banner-form';
import { HeaderComponent } from './header/header';
import { FormErrorMessageComponent } from './form-error-message/form-error-message';

@NgModule({
  declarations: [
    ContactFormComponent,
    BannerFormComponent,
    HeaderComponent,
    FormErrorMessageComponent
  ],
  imports: [IonicModule, CommonModule],
  exports: [
    ContactFormComponent,
    BannerFormComponent,
    HeaderComponent,
    FormErrorMessageComponent
  ]
})
export class ComponentsModule { }
