import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'form-error-message',
  templateUrl: 'form-error-message.html'
})
export class FormErrorMessageComponent {
  private control: FormControl;
  @Input()
  set fControl(control: FormControl) {
    this.control = control;
  }

  constructor() {}
}
