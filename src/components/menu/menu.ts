import { Component, ViewChild } from '@angular/core';
import { Nav, Events, NavController } from 'ionic-angular';
import { HomePublicPage } from '../../pages/home-public/home-public';
import { LoginPage } from '../../pages/login-mbi/login/login';
import { LoginBusinessPage } from '../../pages/login-business/login-business';
import { BenefitListPage } from '../../pages/benefit/benefit-list/benefit-list';
import { AtmAndBranchPage } from '../../pages/localization/atm-and-branch/atm-and-branch';
import { ContactCenterListPage } from '../../pages/contact/contact-center-list/contact-center-list';
import { NewsListPage } from '../../pages/news/news-list/news-list';
import { HelpListPage } from '../../pages/help/help-list/help-list';
import { TermsAndConditionsPage } from '../../pages/terms-and-conditions/terms-and-conditions';

@Component({
  selector: 'menu-component',
  templateUrl: 'menu.html'
})
export class MenuComponent {
  public pages: Array<{ title: string, component: any }>;
  @ViewChild(Nav) nav: Nav;
  rootPage: any = HomePublicPage;

  constructor(public events: Events, public navCtrl: NavController) {
    this.pages = [];
    events.subscribe('menu:opciones', (typeMenu) => {
      this.loadOptionsMenu(typeMenu);
    });
  }

  private loadOptionsMenu(type: number = 0) {
    switch (type) {
      case 0:
        this.pages = [
          { title: 'Inicio', component: HomePublicPage },
          { title: 'Mobile Banking Personas', component: LoginPage },
          { title: 'Mobile Banking Empresas', component: LoginBusinessPage },
          { title: 'Beneficios', component: BenefitListPage },
          { title: 'Cajeros y sucursales', component: AtmAndBranchPage },
          { title: 'Centro de contacto', component: ContactCenterListPage },
          { title: 'Novedades', component: NewsListPage },
          // { title: 'Compartir', component: NewsPage },
          { title: 'Ayuda', component: HelpListPage },
          { title: 'Términos y condiciones', component: TermsAndConditionsPage }
        ];
        break;
      case 1:
        this.pages = [];
        break;
      default:
        break
    }
  }

  // FIXME: No se pued hacer un push de la page en la cual estoy parado
  public openPage(page) {
    this.nav.push(page.component);
  }
}
